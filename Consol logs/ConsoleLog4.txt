run:
****************************************************************************************************
Testing
Algorithm Name: K_NearestNeighbors
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 11
****************************************************************************************************
Duration: 7.6122093540 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.175141% (97.18%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 99.447514% (99.45%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 99.447514% (99.45%)
Digit: 7 -> 97.765363% (97.77%)
Digit: 8 -> 92.528736% (92.53%)
Digit: 9 -> 96.666667% (96.67%)
Global: 97.885364% (97.89%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: K_NearestNeighbors
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 12
****************************************************************************************************
Duration: 6.7096982920 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.740113% (97.74%)
Digit: 3 -> 96.174863% (96.17%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.765363% (97.77%)
Digit: 8 -> 91.954023% (91.95%)
Digit: 9 -> 94.444444% (94.44%)
Global: 97.440178% (97.44%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: K_NearestNeighbors
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 13
****************************************************************************************************
Duration: 6.4807481300 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.175141% (97.18%)
Digit: 3 -> 95.628415% (95.63%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 97.802198% (97.80%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.765363% (97.77%)
Digit: 8 -> 91.954023% (91.95%)
Digit: 9 -> 95.555556% (95.56%)
Global: 97.384530% (97.38%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: K_NearestNeighbors
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 14
****************************************************************************************************
Duration: 6.3200696290 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.175141% (97.18%)
Digit: 3 -> 95.628415% (95.63%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.206704% (97.21%)
Digit: 8 -> 90.229885% (90.23%)
Digit: 9 -> 95.000000% (95.00%)
Global: 97.161937% (97.16%)
****************************************************************************************************
