run:
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 1
Weight Function Version: 1
****************************************************************************************************
Duration: 7.4666307900 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 2
Weight Function Version: 1
****************************************************************************************************
Duration: 6.8912540510 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 3
Weight Function Version: 1
****************************************************************************************************
Duration: 6.6543395970 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 95.000000% (95.00%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 4
Weight Function Version: 1
****************************************************************************************************
Duration: 6.3148068080 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.360656% (98.36%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 96.111111% (96.11%)
Global: 98.219254% (98.22%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 5
Weight Function Version: 1
****************************************************************************************************
Duration: 7.2837537480 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.360656% (98.36%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 96.666667% (96.67%)
Global: 98.274903% (98.27%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 6
Weight Function Version: 1
****************************************************************************************************
Duration: 7.5682701010 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 96.666667% (96.67%)
Global: 98.330551% (98.33%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 7
Weight Function Version: 1
****************************************************************************************************
Duration: 7.8123367080 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 96.666667% (96.67%)
Global: 98.330551% (98.33%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 8
Weight Function Version: 1
****************************************************************************************************
Duration: 7.6924582430 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 96.666667% (96.67%)
Global: 98.386199% (98.39%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 9
Weight Function Version: 1
****************************************************************************************************
Duration: 6.4440815110 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.386199% (98.39%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 10
Weight Function Version: 1
****************************************************************************************************
Duration: 6.6369348900 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.678161% (93.68%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.386199% (98.39%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 11
Weight Function Version: 1
****************************************************************************************************
Duration: 6.8230774350 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.330551% (98.33%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 12
Weight Function Version: 1
****************************************************************************************************
Duration: 6.7277187930 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.330551% (98.33%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 13
Weight Function Version: 1
****************************************************************************************************
Duration: 6.7702617980 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.305085% (98.31%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.274903% (98.27%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 14
Weight Function Version: 1
****************************************************************************************************
Duration: 6.7465126100 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.305085% (98.31%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.274903% (98.27%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 15
Weight Function Version: 1
****************************************************************************************************
Duration: 6.9526502190 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 100.000000% (100.00%)
Digit: 2 -> 98.305085% (98.31%)
Digit: 3 -> 98.907104% (98.91%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.324022% (98.32%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.274903% (98.27%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 20
Weight Function Version: 1
****************************************************************************************************
Duration: 7.1154866810 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.305085% (98.31%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.765363% (97.77%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 98.052309% (98.05%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 30
Weight Function Version: 1
****************************************************************************************************
Duration: 7.1740863360 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.740113% (97.74%)
Digit: 3 -> 97.267760% (97.27%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.206704% (97.21%)
Digit: 8 -> 93.103448% (93.10%)
Digit: 9 -> 97.222222% (97.22%)
Global: 97.885364% (97.89%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 40
Weight Function Version: 1
****************************************************************************************************
Duration: 7.1621202640 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 96.610169% (96.61%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 97.802198% (97.80%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.206704% (97.21%)
Digit: 8 -> 91.954023% (91.95%)
Digit: 9 -> 96.666667% (96.67%)
Global: 97.607123% (97.61%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 50
Weight Function Version: 1
****************************************************************************************************
Duration: 7.4861001970 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 97.175141% (97.18%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 97.802198% (97.80%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.206704% (97.21%)
Digit: 8 -> 91.954023% (91.95%)
Digit: 9 -> 96.666667% (96.67%)
Global: 97.662771% (97.66%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NN
Test Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 100
Weight Function Version: 1
****************************************************************************************************
Duration: 7.6992435970 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 96.610169% (96.61%)
Digit: 3 -> 97.267760% (97.27%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 97.802198% (97.80%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 97.206704% (97.21%)
Digit: 8 -> 90.804598% (90.80%)
Digit: 9 -> 95.000000% (95.00%)
Global: 97.273233% (97.27%)
****************************************************************************************************
