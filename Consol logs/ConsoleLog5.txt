run:
****************************************************************************************************
Training
Algorithm Name: Weighted_K_NNOptimized
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 1
****************************************************************************************************
100.00%...
Duration: 4.1903362020 S
File saved: ./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-04_23-24-45_Weighted_K_NNOptimized_k1.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: Weighted_K_NNOptimized
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 2
****************************************************************************************************
50.00%...
100.00%...
Duration: 6.1139543320 S
File saved: ./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-04_23-25-03_Weighted_K_NNOptimized_k2.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: Weighted_K_NNOptimized
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 5
****************************************************************************************************
20.00%...
40.00%...
60.00%...
80.00%...
100.00%...
Duration: 15.2041692940 S
File saved: ./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: Weighted_K_NNOptimized
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 10
****************************************************************************************************
10.00%...
20.00%...
30.00%...
40.00%...
50.00%...
60.00%...
70.00%...
80.00%...
90.00%...
100.00%...
Duration: 30.8428890990 S
File saved: ./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-04_23-25-55_Weighted_K_NNOptimized_k10.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: Weighted_K_NNOptimized
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
K-Value: 50
****************************************************************************************************
2.00%...
4.00%...
6.00%...
8.00%...
10.00%...
12.00%...
14.00%...
16.00%...
18.00%...
20.00%...
22.00%...
24.00%...
26.00%...
28.00%...
30.00%...
32.00%...
34.00%...
36.00%...
38.00%...
40.00%...
42.00%...
44.00%...
46.00%...
48.00%...
50.00%...
52.00%...
54.00%...
56.00%...
58.00%...
60.00%...
62.00%...
64.00%...
66.00%...
68.00%...
70.00%...
72.00%...
74.00%...
76.00%...
78.00%...
80.00%...
82.00%...
84.00%...
86.00%...
88.00%...
90.00%...
92.00%...
94.00%...
96.00%...
98.00%...
100.00%...
Duration: 149.2492906080 S
File saved: ./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-04_23-28-30_Weighted_K_NNOptimized_k50.txt
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-24-45_Weighted_K_NNOptimized_k1.txt
K-Value: 1
****************************************************************************************************
Duration: 6.8391626430 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-24-45_Weighted_K_NNOptimized_k1.txt
K-Value: 5
****************************************************************************************************
Duration: 13.0898959090 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-24-45_Weighted_K_NNOptimized_k1.txt
K-Value: 10
****************************************************************************************************
Duration: 12.9672668890 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-24-45_Weighted_K_NNOptimized_k1.txt
K-Value: 100
****************************************************************************************************
Duration: 13.1591532900 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-03_Weighted_K_NNOptimized_k2.txt
K-Value: 1
****************************************************************************************************
Duration: 6.6298724280 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-03_Weighted_K_NNOptimized_k2.txt
K-Value: 5
****************************************************************************************************
Duration: 13.6473579020 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-03_Weighted_K_NNOptimized_k2.txt
K-Value: 10
****************************************************************************************************
Duration: 13.5463883800 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-03_Weighted_K_NNOptimized_k2.txt
K-Value: 100
****************************************************************************************************
Duration: 13.4455723620 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
K-Value: 1
****************************************************************************************************
Duration: 6.9945051390 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
K-Value: 2
****************************************************************************************************
Duration: 13.2492812370 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
K-Value: 5
****************************************************************************************************
Duration: 13.6760459490 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
K-Value: 10
****************************************************************************************************
Duration: 13.4662363230 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-20_Weighted_K_NNOptimized_k5.txt
K-Value: 100
****************************************************************************************************
Duration: 19.0918941200 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-55_Weighted_K_NNOptimized_k10.txt
K-Value: 1
****************************************************************************************************
Duration: 7.9546943940 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-55_Weighted_K_NNOptimized_k10.txt
K-Value: 5
****************************************************************************************************
Duration: 15.4958096880 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-55_Weighted_K_NNOptimized_k10.txt
K-Value: 10
****************************************************************************************************
Duration: 13.7775558530 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-25-55_Weighted_K_NNOptimized_k10.txt
K-Value: 100
****************************************************************************************************
Duration: 16.8715606360 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-28-30_Weighted_K_NNOptimized_k50.txt
K-Value: 1
****************************************************************************************************
Duration: 6.6866934130 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 93.888889% (93.89%)
Global: 97.996661% (98.00%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-28-30_Weighted_K_NNOptimized_k50.txt
K-Value: 5
****************************************************************************************************
Duration: 13.1280431130 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-28-30_Weighted_K_NNOptimized_k50.txt
K-Value: 10
****************************************************************************************************
Duration: 13.3256372250 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
****************************************************************************************************
Testing
Algorithm Name: Weighted_K_NNOptimized
Testing Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tes
Trained Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\KNNMap\2016-02-04_23-28-30_Weighted_K_NNOptimized_k50.txt
K-Value: 100
****************************************************************************************************
Duration: 19.0950400340 S
Digit: 0 -> 100.000000% (100.00%)
Digit: 1 -> 99.450549% (99.45%)
Digit: 2 -> 98.870056% (98.87%)
Digit: 3 -> 97.814208% (97.81%)
Digit: 4 -> 98.342541% (98.34%)
Digit: 5 -> 98.351648% (98.35%)
Digit: 6 -> 100.000000% (100.00%)
Digit: 7 -> 98.882682% (98.88%)
Digit: 8 -> 94.252874% (94.25%)
Digit: 9 -> 95.000000% (95.00%)
Global: 98.107958% (98.11%)
****************************************************************************************************
