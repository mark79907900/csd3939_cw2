run:
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 50
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy -1.0
minAccuacy -1.0
minAccuacy -1.0
minAccuacy -1.0
minAccuacy -1.0
minAccuacy 98.18181818181819
Duration: 7.3284373120 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-40-13_98.18%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-40-13_98.18%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 50
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy -1.0
minAccuacy -1.0
minAccuacy 98.01980198019803
Duration: 5.4163528560 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-40-21_98.02%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-40-22_98.02%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 50
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy 98.09523809523809
Duration: 1.5197297820 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-40-29_98.10%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-40-29_98.10%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 100
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy 98.0392156862745
Duration: 0.9417247550 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-40-35_98.04%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-40-35_98.04%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 100
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy 98.18181818181819
Duration: 2.8433803970 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-40-41_98.18%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-40-41_98.18%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 100
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy 98.0392156862745
Duration: 0.6955577480 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-41-02_98.04%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-41-02_98.04%.txt
****************************************************************************************************
****************************************************************************************************
Training
Algorithm Name: K_Means
Training Path: D:\Dropbox\STC\3rd Year\AI\CSD3939_CW2\Data Set\Data Folder\PreProcessed\optdigits.tra
Centroid: Random
K-Value: 100
Minimum Accuracy Threshold: 98
A value of -1 means that not all of the digits(0..9) where formed into clusters
****************************************************************************************************
minAccuacy 98.11320754716981
Duration: 1.4176495970 S
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/UN_PROCESSED_2016-02-05_17-41-18_98.11%.txt
File saved: ./Data Set/Data Folder/PreProcessed/Centroid/PROCESSED_2016-02-05_17-41-18_98.11%.txt
****************************************************************************************************
BUILD SUCCESSFUL (total time: 1 minute 26 seconds)
