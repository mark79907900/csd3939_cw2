% $ biblatex auxiliary file $
% $ biblatex version 2.4 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\entry{alpaydin1998cascaded}{article}{}
  \name{author}{2}{}{%
    {{}%
     {Alpaydin}{A.}%
     {E}{E}%
     {}{}%
     {}{}}%
    {{}%
     {Kaynak}{K.}%
     {C}{C}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{AEKC1}
  \strng{fullhash}{AEKC1}
  \field{labelyear}{1998}
  \field{sortinit}{A}
  \field{pages}{369\bibrangedash 374}
  \field{title}{Cascaded Classifiers}
  \field{volume}{34}
  \field{journaltitle}{Kybernetika}
  \field{year}{1998}
\endentry

\entry{9_archive.ics.uci.edu_2016}{misc}{}
  \name{author}{1}{}{%
    {{}%
     {Archive.ics.uci.edu}{A.}%
     {}{}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{A1}
  \strng{fullhash}{A1}
  \field{labelyear}{2016}
  \field{sortinit}{A}
  \field{title}{UCI Machine Learning Repository: Optical Recognition of
  Handwritten Digits Data Set}
  \verb{url}
  \verb http://archive.ics.uci.edu/ml/datasets/Optical+Recognition+of_Handwritt
  \verb en+Digits
  \endverb
  \field{year}{2016}
  \field{urlday}{02}
  \field{urlmonth}{06}
  \field{urlyear}{2016}
\endentry

\entry{Moreno-Torres2012}{article}{}
  \name{author}{3}{}{%
    {{}%
     {Moreno-Torres}{M.-T.}%
     {Jose~Garc{\'{\i}}a}{J.~G.}%
     {}{}%
     {}{}}%
    {{}%
     {Saez}{S.}%
     {Jos{\'{e}}~a.}{J.~a.}%
     {}{}%
     {}{}}%
    {{}%
     {Herrera}{H.}%
     {Francisco}{F.}%
     {}{}%
     {}{}}%
  }
  \keyw{Covariate shift,cross-validation,dataset shift,partitioning}
  \strng{namehash}{MTJGSJaHF1}
  \strng{fullhash}{MTJGSJaHF1}
  \field{labelyear}{2012}
  \field{sortinit}{M}
  \field{abstract}{%
  Cross-validation is a very commonly employed technique used to evaluate
  classifier performance. However, it can potentially introduce dataset shift,
  a harmful factor that is often not taken into account and can result in
  inaccurate performance estimation. This paper analyzes the prevalence and
  impact of partition-induced covariate shift on different k-fold
  cross-validation schemes. From the experimental results obtained, we conclude
  that the degree of partition-induced covariate shift depends on the
  cross-validation scheme considered. In this way, worse schemes may harm the
  correctness of a single-classifier performance estimation and also increase
  the needed number of repetitions of cross-validation to reach a stable
  performance estimation.%
  }
  \verb{doi}
  \verb 10.1109/TNNLS.2012.2199516
  \endverb
  \field{isbn}{2162-2388 (Electronic)$\backslash$r2162-237X (Linking)}
  \field{issn}{2162237X}
  \field{number}{8}
  \field{pages}{1304\bibrangedash 1312}
  \field{title}{{Study on the impact of partition-induced dataset shift on
  k-fold cross-validation}}
  \verb{url}
  \verb http://ieeexplore.ieee.org/xpls/abs{\_}all.jsp?arnumber=6226477
  \endverb
  \field{volume}{23}
  \verb{file}
  \verb :D$\backslash$:/odrive/MDX Google Drive/Year 2 Personal Files/CSD 3997
  \verb Computer Science (Systems Engineering) Project/Papers/n-fold/10.1109TNN
  \verb LS.2012.2199516.pdf:pdf
  \endverb
  \field{journaltitle}{IEEE Transactions on Neural Networks and Learning
  Systems}
  \field{year}{2012}
\endentry

\entry{Xie2009}{article}{}
  \name{author}{6}{}{%
    {{}%
     {Xie}{X.}%
     {Xiaoyuan}{X.}%
     {}{}%
     {}{}}%
    {{}%
     {Ho}{H.}%
     {Joshua}{J.}%
     {}{}%
     {}{}}%
    {{}%
     {Murphy}{M.}%
     {Christian}{C.}%
     {}{}%
     {}{}}%
    {{}%
     {Kaiser}{K.}%
     {Gail}{G.}%
     {}{}%
     {}{}}%
    {{}%
     {Xu}{X.}%
     {Baowen}{B.}%
     {}{}%
     {}{}}%
    {{}%
     {Chen}{C.}%
     {Tsong~Yueh}{T.~Y.}%
     {}{}%
     {}{}}%
  }
  \strng{namehash}{XX+1}
  \strng{fullhash}{XXHJMCKGXBCTY1}
  \field{labelyear}{2009}
  \field{sortinit}{X}
  \field{abstract}{%
  Many applications in the field of scientific computing - such as
  computational biology, computational linguistics, and others - depend on
  Machine Learning algorithms to provide important core functionality to
  support solutions in the particular problem domains. However, it is difficult
  to test such applications because often there is no "test oracle" to indicate
  what the correct output should be for arbitrary input. To help address the
  quality of such software, in this paper we present a technique for testing
  the implementations of supervised machine learning classification algorithms
  on which such scientific computing software depends. Our technique is based
  on an approach called "metamorphic testing", which has been shown to be
  effective in such cases. More importantly, we demonstrate that our technique
  not only serves the purpose of verification, but also can be applied in
  validation. In addition to presenting our technique, we describe a case study
  we performed on a real-world machine learning application framework, and
  discuss how programmers implementing machine learning algorithms can avoid
  the common pitfalls discovered in our study. We also discuss how our findings
  can be of use to other areas outside scientific computing, as well.%
  }
  \verb{doi}
  \verb 10.1109/QSIC.2009.26
  \endverb
  \field{isbn}{9780769538280}
  \field{issn}{15506002}
  \field{pages}{135\bibrangedash 144}
  \field{title}{{Application of metamorphic testing to supervised classifiers}}
  \verb{file}
  \verb :C$\backslash$:/Users/Mark/AppData/Local/Mendeley Ltd./Mendeley Desktop
  \verb /Downloaded/Xie et al. - 2009 - Application of metamorphic testing to s
  \verb upervised classifiers.pdf:pdf
  \endverb
  \field{journaltitle}{Proceedings - International Conference on Quality
  Software}
  \field{year}{2009}
\endentry

\lossort
\endlossort

\endinput
