\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Acronyms}{2}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Quality of Code}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Graphical User Interface(GUI)}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Configuration File}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Object-oriented programming (OOP) and Reusability}{3}{subsubsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Utils Package}{3}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Centralised Error Handling}{3}{subsubsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2}Results Class}{3}{subsubsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3}Files Class}{4}{subsubsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}DataTypes Package}{4}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1}AlgorithmType Enum}{4}{subsubsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2}Image Class}{4}{subsubsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}The Dataset}{5}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Quality of Algorithm and Results}{6}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Nearest Neighbour(NN)}{6}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Results}{6}{subsubsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Evaluation}{6}{subsubsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}K-Nearest Neighbour(K-NN)}{7}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Results}{7}{subsubsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Graph}{8}{subsubsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}Evaluation}{8}{subsubsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}K-NN Optimized}{9}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.1}Training}{9}{subsubsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.2}Testing}{9}{subsubsection.4.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.3}Results}{9}{subsubsection.4.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.4}Graphs}{11}{subsubsection.4.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.3.5}Evaluation}{12}{subsubsection.4.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Weighted K-NN}{13}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.1}Results}{14}{subsubsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.2}Graphs}{16}{subsubsection.4.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.4.3}Evaluation}{16}{subsubsection.4.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Optimized K-NN with Weighted Functions}{18}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.1}Results}{18}{subsubsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.2}Graphs}{20}{subsubsection.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.3}Evaluation}{21}{subsubsection.4.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.6}Weighted Parzen Windows}{22}{subsection.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.1}Results}{22}{subsubsection.4.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.2}Graphs}{24}{subsubsection.4.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.6.3}Evaluation}{24}{subsubsection.4.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7}K-Means}{26}{subsection.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.1}Testing}{26}{subsubsection.4.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.2}Training}{26}{subsubsection.4.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.3}Results}{26}{subsubsection.4.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.4}Graphs}{28}{subsubsection.4.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.7.5}Evaluation}{28}{subsubsection.4.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.8}Optimized K-Means}{30}{subsection.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.8.1}Results}{30}{subsubsection.4.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.8.2}Evaluation}{30}{subsubsection.4.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Conclusion}{31}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Repository}{31}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}More Results}{34}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}References}{37}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}\ignorespaces Bibliography}{38}{section.8}
