/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.DataTypes;

import csd3939_cw2.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Holds all the points of an image together with the digit value
 * @author Mark
 */
public class Image {
    private ArrayList<ImagePoint> imagepointList;
    private int digit;
    
    public Image() {
        this.imagepointList = new ArrayList<ImagePoint>();
    }
    
    /**
     * Add 64 entries with value of 0 representing a blank image
     */
    public void setBlank(){
        for(int i=0;i<64;i++){
            addPoint(new ImagePoint(0));
        }
    }
    
    /**
     * add point to image point list
     * @param point
     */
    public void addPoint(ImagePoint point){
        this.imagepointList.add(point);
    }
    
    /**
     * Set the digit value
     * @param digit
     */
    public void setDigit(int digit){
        this.digit = digit;
    }
    
    /**
     * get the number of points for this image
     * @return
     */
    public int getNumberOfPoints(){
        return this.imagepointList.size();
    }
    
    /**
     * Get the value of the point for the given index
     * @param index
     * @return
     */
    public double getValue(int index){
        return this.imagepointList.get(index).getDouble();
    }
    
    /**
     * Set the value of a particular image point
     * @param index
     * @param value 
     */
    public void setValue(int index, double value, boolean validate){
        try{
            ImagePoint p = new ImagePoint(value, validate);
            this.imagepointList.set(index, p);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * get the digit value
     * @return
     */
    public int getDigit(){
        return digit;
    }
    
    /**
     * Get the entire list of image points
     * @return
     */
    public ArrayList<ImagePoint> getImagePoints(){
        return this.imagepointList;
    }
    
    /**
     * replace the old image points list with a new one
     * @param imagePointsList 
     */
    public void setImagePoints(ArrayList<ImagePoint> imagePointsList){
        this.imagepointList = new ArrayList(imagePointsList);
    }
    
}
