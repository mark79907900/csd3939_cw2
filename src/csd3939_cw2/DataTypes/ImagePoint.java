/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.DataTypes;

import csd3939_cw2.Utils.ErrorHandling;
import java.util.Random;
import java.util.logging.Level;


/**
 * holds the points of the images
 * @author Mark
 */
public class ImagePoint{
    private double point;
    
    public ImagePoint(double point) {
        this(point, true);
    }
    
    public ImagePoint(double point, boolean validate) {
        if(validate){
            if(point <= 16 && point >= 0){
                this.point = point;
            }else{
                try{
                    ErrorHandling.throwException(String.format("Input point is greater then 16: %d", point));
                }catch(Exception ex){
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        }else{
            this.point = point;
        }
        
    }
    
    public ImagePoint(){
        Random rand = new Random();
        double point = rand.nextInt(17-0) + 0;
        
        if(point <= 16 && point >= 0){
            this.point = point;
        }else{
            try{
                ErrorHandling.throwException(String.format("Input point is greater then 16: %d", point));
            }catch(Exception ex){
                ErrorHandling.output(Level.SEVERE, ex);
            }
        }
    }
    
    /**
     * Method used to change the point value
     * @param point
     */
    public void setValue(double point){
        try{
            if(point >= 0 && point <= 16){
                this.point = point;
            }else{
                ErrorHandling.throwException(String.format("ImagePoint must be within the limit of 0 to 16, passed value : %f", point));
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * get the integer value of the point
     * @return
     */
    public double getDouble(){
        return this.point;
    }
    
    @Override
    public String toString(){
        return String.format("%f", this.point);
    }
 
}
