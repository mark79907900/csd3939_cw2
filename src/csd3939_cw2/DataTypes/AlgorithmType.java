package csd3939_cw2.DataTypes;

import java.util.stream.Stream;

/**
 * The list of available algorithms
 * @author Mark
 */
public enum AlgorithmType {
    NearestNeighbors,
    K_NearestNeighbors,
    K_NNOptimized,
    Weighted_K_NN,
    Weighted_K_NNOptimized,
    WeightedParzenWindows,
    K_Means,
    K_MeansOptimised;
    
    
    /**
     * Returns the names of all the types of this enum
     * Implementation could be found on
     * //http://stackoverflow.com/questions/13783295/getting-all-names-in-an-enum-as-a-string
     * @return
     */
    public static String[] getNames(){
        return Stream.of(AlgorithmType.values()).map(AlgorithmType::name).toArray(String[]::new);
    }
}
