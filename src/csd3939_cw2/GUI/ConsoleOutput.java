package csd3939_cw2.GUI;

import csd3939_cw2.Algorithms.KMeans.Centroid;
import csd3939_cw2.Algorithms.KMeans.KMeansTesting;
import csd3939_cw2.Algorithms.KMeans.KMeansTraning;
import csd3939_cw2.Algorithms.KMeans.K_MeansTraningOptimised;
import csd3939_cw2.Algorithms.KNN.Accuracy;
import csd3939_cw2.Algorithms.KNN.K_NN;
import csd3939_cw2.Algorithms.KNN.K_NNOptimizedTesting;
import csd3939_cw2.Algorithms.KNN.K_NNOptimizedTraining;
import csd3939_cw2.Algorithms.KNN.WeightedK_NNOptimizedTesting;
import csd3939_cw2.Algorithms.KNN.WeightedK_NNOptimizedTraining;
import csd3939_cw2.Algorithms.KNN.Weighted_K_NN;
import csd3939_cw2.Algorithms.WeightedParzenWindows;
import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Algorithms.NN;
import csd3939_cw2.Utils.Config;
import csd3939_cw2.Utils.ErrorHandling;
import csd3939_cw2.Utils.Files;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class ConsoleOutput implements IExecuteAlgorithm{
    
    public ConsoleOutput() {
        //setup the gui
        MainGui.init(this);
    }
    
    /**
     * Method used to test the selected algorithm that require a testing and training set
     * @param testPath
     * @param trainingPath
     * @param algorithmType
     * @param kValue
     */
    @Override
    public void test(AlgorithmType algorithmType, String testPath, String trainingPath, int kValue) {
        try {
            //load the lists of image points in memory
            ArrayList<Image> testingSet = Files.readImageFromTextFile(testPath);
            ArrayList<Image> trainingSet = Files.readImageFromTextFile(trainingPath);
            
            printDividers(100);
            System.out.println(String.format("%s%s%s", "\u001B[31m", "Testing", "\u001B[0m"));
            
            //Algorithm Name
            System.out.println(
                    String.format("%s %s%s%s",
                            "Algorithm Name:",
                            "\u001B[34m", algorithmType.toString(), "\u001B[0m"
                    ));
            
            //path
            System.out.println(String.format("Test Path: %s%s%s", "\u001B[34m", testPath, "\u001B[0m"));
            System.out.println(String.format("Training Path: %s%s%s", "\u001B[34m", trainingPath, "\u001B[0m"));
            
            //algorithm selector switch case
            switch(algorithmType){
                case NearestNeighbors:{
                    printDividers(100);
                    NN nn = new NN(testingSet, trainingSet);
                    nn.execute();
                    break;
                }
                case K_NearestNeighbors:{
                    //k-value
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    K_NN k_nn = new K_NN(testingSet, trainingSet, kValue);
                    k_nn.execute();
                    k_nn.print();
                    break;
                }
                case Weighted_K_NN:{
                    //k-value
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    System.out.println(String.format("Weight Function Version: %s%d%s", "\u001B[34m", Config.getPropertyInt("weightedKNNVersion"), "\u001B[0m"));
                    printDividers(100);
                    Weighted_K_NN k_nn = new Weighted_K_NN(testingSet, trainingSet, kValue);
                    k_nn.execute();
                    k_nn.print();
                    break;
                }
                case WeightedParzenWindows:{
                    //k-value
                    System.out.println(String.format("RMS Threshold: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    System.out.println(String.format("Weight Function(ON/OFF): %s%d%s", "\u001B[34m", Config.getPropertyInt("weightedOFFPWVersion"), "\u001B[0m"));
                    printDividers(100);
                    WeightedParzenWindows k_nn = new WeightedParzenWindows(testingSet, trainingSet, kValue);
                    k_nn.execute();
                    k_nn.print();
                    break;
                }
                default:{
                    ErrorHandling.throwException("Algorithm NOT supported yet!");
                    break;
                }
            }
            
            printDividers(100);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * This method is used for algorithms which require a centroid and a testing set
     * @param algorithmType
     * @param kValue
     * @param testPath
     * @param centroidPath
     */
    @Override
    public void test(AlgorithmType algorithmType, int kValue, String testPath, String centroidPath) {
        try{
            //load the lists of image points in memory
            ArrayList<Image> testingSet = Files.readImageFromTextFile(testPath);
            ArrayList<Centroid> centroidSet = Files.readCentroidFromTextFile(centroidPath, kValue);
            
            printDividers(100);
            System.out.println(String.format("%s%s%s", "\u001B[31m", "Testing", "\u001B[0m"));
            
            //Algorithm Name
            System.out.println(
                    String.format("%s %s%s%s",
                            "Algorithm Name:",
                            "\u001B[34m", algorithmType.toString(), "\u001B[0m"
                    ));
            
            //path
            System.out.println(String.format("Test Path: %s%s%s", "\u001B[34m", testPath, "\u001B[0m"));
            System.out.println(String.format("Centroid Path: %s%s%s", "\u001B[34m", centroidPath, "\u001B[0m"));
            
            //algorithm selector switch case
            switch(algorithmType){
                case K_Means:{
                    printDividers(100);
                    KMeansTesting k_means = new KMeansTesting(testingSet, centroidSet, kValue);
                    k_means.execute();
                    break;
                }
                case K_MeansOptimised:{
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    KMeansTesting k_means = new KMeansTesting(testingSet, centroidSet, kValue);
                    k_means.execute();
                    break;
                }
                default:{
                    ErrorHandling.throwException("Algorithm NOT supported yet!");
                    break;
                }
            }
            
            printDividers(100);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    @Override
    public void test(AlgorithmType algorithmType, String testPath, String trainingPath, String trainedPath, int kValue) {
        try{
            //load the lists of image points in memory
            ArrayList<Image> testingSet = Files.readImageFromTextFile(testPath);
            ArrayList<Image> trainingSet = Files.readImageFromTextFile(trainingPath);
            ArrayList<Accuracy> accuracySet = Files.readKNNMapFromTextFile(trainedPath);
            
            printDividers(100);
            System.out.println(String.format("%s%s%s", "\u001B[31m", "Testing", "\u001B[0m"));
            
            //Algorithm Name
            System.out.println(
                    String.format("%s %s%s%s",
                            "Algorithm Name:",
                            "\u001B[34m", algorithmType.toString(), "\u001B[0m"
                    ));
            
            //path
            System.out.println(String.format("Testing Path: %s%s%s", "\u001B[34m", testPath, "\u001B[0m"));
            System.out.println(String.format("Training Path: %s%s%s", "\u001B[34m", testPath, "\u001B[0m"));
            System.out.println(String.format("Trained Path: %s%s%s", "\u001B[34m", trainedPath, "\u001B[0m"));
            
            //algorithm selector switch case
            switch(algorithmType){
                case K_NNOptimized:{
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    K_NNOptimizedTesting k_nn = new K_NNOptimizedTesting(
                            testingSet,
                            trainingSet,
                            accuracySet,
                            kValue
                    );
                    k_nn.execute();
                    break;
                }
                case Weighted_K_NNOptimized:{
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    WeightedK_NNOptimizedTesting k_nn = new WeightedK_NNOptimizedTesting(
                            testingSet,
                            trainingSet,
                            accuracySet,
                            kValue
                    );
                    k_nn.execute();
                    break;
                }
                default:{
                    ErrorHandling.throwException("Algorithm NOT supported yet!");
                    break;
                }
            }
            
            printDividers(100);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * Method used to invoke train methods for some algorithm
     * @param algorithmType
     * @param testPath
     * @param trainingPath
     * @param kValue
     */
    @Override
    public void train(AlgorithmType algorithmType, String trainingPath, String centroidPath, int kValue) {
        try {
            //load the lists of image points in memory
            ArrayList<Image> trainingSet = Files.readImageFromTextFile(trainingPath);
            ArrayList<Centroid> centroidSet = null;
            
            printDividers(100);
            System.out.println(String.format("%s%s%s", "\u001B[31m", "Training", "\u001B[0m"));
            
            //Algorithm Name
            System.out.println(
                    String.format("%s %s%s%s",
                            "Algorithm Name:",
                            "\u001B[34m", algorithmType.toString(), "\u001B[0m"
                    ));
            
            //path
            System.out.println(String.format("Training Path: %s%s%s", "\u001B[34m", trainingPath, "\u001B[0m"));
            
            
            //algorithm selector switch case
            switch(algorithmType){
                case K_NNOptimized:{
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    K_NNOptimizedTraining k_nn = new K_NNOptimizedTraining(trainingSet, kValue);
                    k_nn.execute();
                    break;
                }
                case Weighted_K_NNOptimized:{
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    printDividers(100);
                    WeightedK_NNOptimizedTraining k_nn = new WeightedK_NNOptimizedTraining(trainingSet, kValue);
                    k_nn.execute();
                    break;
                }
                case K_Means:{
                    if(centroidPath == null){
                        System.out.println(String.format("Centroid: %s%s%s", "\u001B[34m", "Random", "\u001B[0m"));
                    }else{
                        centroidSet = Files.readCentroidFromTextFile(centroidPath, kValue);
                        System.out.println(String.format("Centroid Path: %s%s%s", "\u001B[34m", centroidPath, "\u001B[0m"));
                    }
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", kValue, "\u001B[0m"));
                    System.out.println(String.format("Minimum Accuracy Threshold: %s%s%s", "\u001B[34m", Config.getPropertyInt("minimumAccuracyThreshold"), "\u001B[0m"));
                    System.out.println(String.format("%s%s%s", "\u001B[31m", "A value of -1 means that not all of the digits(0..9) where formed into clusters", "\u001B[0m"));
                    printDividers(100);
                    KMeansTraning k_means = new KMeansTraning(trainingSet, centroidSet, kValue);
                    k_means.execute();
                    break;
                }
                case K_MeansOptimised:{
                    //k-value
                    System.out.println(String.format("Centroid: %s%s%s", "\u001B[34m", "Mean", "\u001B[0m"));
                    System.out.println(String.format("K-Value: %s%s%s", "\u001B[34m", 10, "\u001B[0m"));
                    printDividers(100);
                    K_MeansTraningOptimised k_means = new K_MeansTraningOptimised(trainingSet, centroidSet, 10);
                    k_means.execute();
                    break;
                }
                
                default:{
                    ErrorHandling.throwException("Algorithm NOT supported yet!");
                    break;
                }
            }
            
            printDividers(100);
        } catch (Exception ex) {
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * method used to print dividers between solutions
     * @param length number of stars
     */
    private void printDividers(int length){
        for(int i=0;i<length;i++){
            System.out.print("\u001B[33m*");
        }
        System.out.println("\u001B[0m");
    }
}
