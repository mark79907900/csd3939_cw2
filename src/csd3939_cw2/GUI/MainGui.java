package csd3939_cw2.GUI;

import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.Utils.Config;
import csd3939_cw2.Utils.ErrorHandling;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.logging.Level;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author Mark
 */
public class MainGui {
    private static MainGui self = null;
    
    private final MainWindow mw;
    private JPanel top;
    private JPanel bottom;
    private String testingPath, trainingPath, trainedPath;
    private enum FieldType{TESTING, TRAINING, CENTROID};
    
    /**
     * This method is used to init the GUI and ensure that only one instance can be created (Singleton)
     * @param callBack
     */
    public static void init(IExecuteAlgorithm callBack){
        if (self == null){
            self = new MainGui(callBack);
        }
    }
    
    /**
     * private Constructor to build the GUI
     * It creates a new Window and sets up the GUI components by invoking the private method setup
     * @param callBack
     */
    private MainGui(IExecuteAlgorithm callBack) {
        this.mw = MainWindow.getMainWindow(Config.getPropertyString("mainWindowName"));
        setup(callBack);
    }
    
    /**
     * Creates the Components
     * @param callBack
     */
    private void setup(IExecuteAlgorithm callBack){
        try{
            //Make the top panel which will hold the algorithm selection
            this.top = new JPanel(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            
            //label for the combobox
            JLabel label = new JLabel(Config.getPropertyString("selectAlgorithm"));
            label.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
            label.setFont(new Font("Serif", Font.PLAIN, 20));
            
            c.gridx = 0;
            c.gridy = 0;
            c.weightx = 0.2;
            this.top.add(label, c);
            
            //combobox used to select the algoritm to be used.
            //The list of avialable options is being gathered from the AlgorithmType types
            String[] algorithms = AlgorithmType.getNames();
            JComboBox algorithmsComboBox = new JComboBox(algorithms);
            algorithmsComboBox.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
            algorithmsComboBox.setSelectedIndex(0);
            
            //add the combo box to the panel
            c.gridx = 1;
            c.gridy = 0;
            this.top.add(algorithmsComboBox, c);
            
            //K-Value label and text box
            JLabel kValueLabel = new JLabel(Config.getPropertyString("kValueLabel"));
            kValueLabel.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 10));
            kValueLabel.setFont(new Font("Serif", Font.PLAIN, 20));
            c.gridx = 0;
            c.gridy = 1;
            c.weightx = 0.2;
            this.top.add(kValueLabel, c);
            
            SpinnerModel sm = new SpinnerNumberModel(Config.getPropertyInt("defaultKValue"), 1, Integer.MAX_VALUE, 1);
            JSpinner kField = new JSpinner(sm);
            c.gridx = 1;
            c.gridy = 1;
            this.top.add(kField, c);
            
            //make the fileChooser
            JFileChooser fileChooser = new JFileChooser();
            getFile(2, c, fileChooser, this.top, FieldType.TESTING);
            getFile(3, c, fileChooser, this.top, FieldType.TRAINING);
            JTextField trainedField = getFile(4, c, fileChooser, this.top, FieldType.CENTROID);
            
            //add the panel to the main window
            this.mw.add(this.top, BorderLayout.NORTH);
            
            //make the bottom panel which will hold the file selection
            this.bottom = new JPanel();
            
            //test button
            JButton testButton = new JButton(Config.getPropertyString("testButton"));
            testButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    
                    if(callBack != null){
                        switch(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem())){
                            case K_Means:
                            case K_MeansOptimised:{
                                if(testingTrainedValidation()){
                                    int kValue = getKValue(kField);
                                    
                                    if(kValue > 0){
                                        //delegete the event
                                        callBack.test(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem()),
                                                kValue,
                                                testingPath,
                                                trainedPath
                                        );
                                    }
                                }
                                break;
                            }
                            case K_NNOptimized:
                            case Weighted_K_NNOptimized:{
                                if(testingTrainedValidation() && testingTrainingValidation()){
                                    int kValue = getKValue(kField);
                                    
                                    if(kValue > 0){
                                        //delegete the event
                                        callBack.test(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem()),
                                                testingPath,
                                                trainingPath,
                                                trainedPath,
                                                kValue
                                        );
                                    }
                                }
                                break;
                            }
                            default:{
                                if(testingTrainingValidation()){
                                    int kValue = getKValue(kField);
                                    
                                    if(kValue > 0){
                                        //delegete the event
                                        callBack.test(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem()),
                                                testingPath,
                                                trainingPath,
                                                kValue
                                        );
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            });
            
            //Training button
            JButton trainButton = new JButton(Config.getPropertyString("trainButton"));
            trainButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    
                    //training path validation
                    if(trainingPath == null){
                        JOptionPane.showMessageDialog(mw, Config.getPropertyString("trainingNullMessage"),
                                "Inane error",
                                JOptionPane.ERROR_MESSAGE);
                    }else{
                        if(callBack != null){
                            int kValue = getKValue(kField);
                            
                            //if k-value is 0 prevent the user from continuing
                            if(kValue > 0){
                                //delegete the event
                                callBack.train(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem()),
                                        trainingPath,
                                        trainedPath,
                                        kValue
                                );
                            }
                        }
                    }
                }
            });
            
            //add the buttons
            this.bottom.add(trainButton);
            this.bottom.add(testButton);
            
            //if the default selection is set to NearestNeighbors, the K-Value is disabled
            if(AlgorithmType.valueOf((String)algorithmsComboBox.getSelectedItem()) == AlgorithmType.NearestNeighbors){
                kValueLabel.setEnabled(false);
                kField.setEnabled(false);
                trainButton.setEnabled(false);
//                centroidButton.setEnabled(false);
                trainedField.setBackground(Color.LIGHT_GRAY);
            }
            
            //switch gui functionality based on algorithm selection
            algorithmsComboBox.addActionListener(
                    new ActionListener(){
                        public void actionPerformed(ActionEvent e){
                            try{
                                JComboBox combo = (JComboBox)e.getSource();
                                AlgorithmType selection =  AlgorithmType.valueOf((String)combo.getSelectedItem());
                                
                                switch(selection){
                                    case NearestNeighbors:{
                                        kValueLabel.setEnabled(false);
                                        kField.setEnabled(false);
                                        kField.setValue(1);
                                        kValueLabel.setText(Config.getPropertyString("kValueLabel"));
                                        trainButton.setEnabled(false);
                                        trainedField.setBackground(Color.LIGHT_GRAY);
//                                        centroidButton.setEnabled(false);
                                        break;
                                    }
                                    case K_NearestNeighbors:
                                    case Weighted_K_NN:{
                                        kValueLabel.setEnabled(true);
                                        kField.setEnabled(true);
                                        kField.setValue(Config.getPropertyInt("defaultKValue"));
                                        kValueLabel.setText(Config.getPropertyString("kValueLabel"));
                                        trainButton.setEnabled(false);
//                                        centroidButton.setEnabled(false);
                                        trainedField.setBackground(Color.LIGHT_GRAY);
                                        break;
                                    }
                                    case K_NNOptimized:
                                    case Weighted_K_NNOptimized:{
                                        kValueLabel.setEnabled(true);
                                        kField.setEnabled(true);
                                        kField.setValue(Config.getPropertyInt("allKnnDefaultValues"));
                                        kValueLabel.setText(Config.getPropertyString("nValueLabel"));
                                        trainButton.setEnabled(true);
//                                        centroidButton.setEnabled(true);
                                        trainedField.setBackground(Color.WHITE);
                                        break;
                                    }
                                    case K_Means:{
                                        kValueLabel.setEnabled(true);
                                        kField.setEnabled(true);
                                        kField.setValue(Config.getPropertyInt("KMeansDefaultValue"));
                                        kValueLabel.setText(Config.getPropertyString("kValueLabel"));
                                        trainButton.setEnabled(true);
//                                        centroidButton.setEnabled(true);
                                        trainedField.setBackground(Color.WHITE);
                                        break;
                                    }
                                    case K_MeansOptimised:{
                                        kValueLabel.setEnabled(false);
                                        kField.setEnabled(false);
                                        kField.setValue(10);
                                        kValueLabel.setText(Config.getPropertyString("kValueLabel"));
                                        trainButton.setEnabled(true);
                                        trainedField.setBackground(Color.WHITE);
//                                        centroidButton.setEnabled(true);
                                        break;
                                    }
                                    case WeightedParzenWindows:{
                                        kValueLabel.setEnabled(true);
                                        kField.setEnabled(true);
                                        kField.setValue(Config.getPropertyInt("parzWindowsDefaultRMSValue"));
                                        kValueLabel.setText(Config.getPropertyString("rmsValueLabel"));
                                        trainButton.setEnabled(false);
//                                        centroidButton.setEnabled(false);
                                        trainedField.setBackground(Color.LIGHT_GRAY);
                                        break;
                                    }
                                    default:{
                                        ErrorHandling.throwException("Algorithm NOT supported yet!");
                                        break;
                                    }
                                }
                            }catch(Exception ex){
                                ErrorHandling.output(Level.SEVERE, ex);
                            }
                        }
                    }
            );
            
            //add the panel to the main window
            this.mw.add(this.bottom, BorderLayout.SOUTH);
            
            //set the window visiable
            this.mw.setVisible();
            this.mw.setResizable(false);
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * Get the KValue from the Text field and validate the value
     * @param kField
     * @return
     */
    private int getKValue(JSpinner kField){
        int kValue = -1;
        int kFieldValue = -1;
        
        try{
            if(kField != null){
                kFieldValue = (int)kField.getValue();
                
                //check if empty, and return default
                if(kFieldValue == 0){
                    JOptionPane.showMessageDialog(mw, Config.getPropertyString("kValueZeroMessage"),
                            "Inane error",
                            JOptionPane.ERROR_MESSAGE);
                    return kValue;
                }else{
                    return (int)kField.getValue();
                }
            }
            else{
                ErrorHandling.throwException("The textfield passed for the K Value is null");
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return kValue;
    }
    
    /**
     * This is the validation which should be carried out for NN and K-NN
     * It validates both testing and training sets
     * @return
     */
    private boolean testingTrainingValidation(){
        //file path validation
        if(testingPath == null && trainingPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingTrainingNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }
        else if(testingPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(trainingPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("trainingNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(testingPath.equals(trainingPath)){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingEqualTrainingMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(testingPath != null && trainingPath != null){
            return true;
        }
        return false;
    }
    
    /**
     * This is the validation which should be carried out for K-Means
     * It validates both testing and centroid sets
     * @return
     */
    private boolean testingTrainedValidation(){
        //file path validation
        if(testingPath == null && trainedPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingTrainedNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }
        else if(testingPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(trainedPath == null){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("trainedNullMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(testingPath.equals(trainedPath)){
            JOptionPane.showMessageDialog(mw, Config.getPropertyString("testingEqualTrainedgMessage"),
                    "Inane error",
                    JOptionPane.ERROR_MESSAGE);
        }else if(testingPath != null && trainedPath != null){
            return true;
        }
        return false;
    }
    
    /**
     * Generate the file chooser and add it to the passed panel
     * This Works only with a GridBagConstraints
     * @param row the row index where the item should be placed in the panel
     * @param c GridBagConstraints
     * @param fileChooser JFileChooser
     * @param panel JPanel
     * @param fieldType Path Enum which is used to switch between the testing, training and centroid set. This reduces the number of parameters needed to be passed to this method
     */
    private JTextField getFile(int row, GridBagConstraints c, JFileChooser fileChooser, JPanel panel, FieldType fieldType){
        final JTextField Name = new JTextField(15);
        
        try{
            //get the button name and added to the panel
            String labelText = "";
            
            switch(fieldType){
                case TESTING:{
                    labelText = Config.getPropertyString("testFileLabel");
                    break;
                }
                case TRAINING:{
                    labelText = Config.getPropertyString("trainingFileLabel");
                    break;
                }
                case CENTROID:{
                    labelText = Config.getPropertyString("centroidFileLabel");
                    break;
                }
                default:{
                    break;
                }
            }
            
            //add the button and text field
            JLabel label = new JLabel(labelText);
            c.gridx = 0;
            c.gridy = row;
            panel.add(label, c);
            
            Name.setEditable(false);
            Name.setBackground(Color.WHITE);
            c.gridx = 1;
            c.gridy = row;
            panel.add(Name, c);
            
            JButton selectButton = new JButton("X");
            c.insets = new Insets(2, 0, 2, 0);
            c.gridx = 2;
            c.gridy = row;
            panel.add(selectButton, c);
            
            //attach an action listener to the button
            selectButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try{
                        //get the file path
                        switch(fieldType){
                            case TESTING:{
                                testingPath = null;
                                Name.setText("");
                                break;
                            }
                            case TRAINING:{
                                trainingPath = null;
                                Name.setText("");
                                break;
                            }
                            case CENTROID:{
                                trainedPath = null;
                                Name.setText("");
                                break;
                            }
                            default:{
                                break;
                            }
                        }
                        
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
            });
            
            //listner which will remove the item in the field when clicked
            Name.addMouseListener(new MouseAdapter(){
                @Override
                public void mouseClicked(MouseEvent  e) {
                    try{
                        //set the default directory
                        fileChooser.setCurrentDirectory(new File(Config.getPropertyString("fileChooserDefaultDirectory")));
                        
                        //check for a valid selection
                        if (fileChooser.showOpenDialog(mw) == JFileChooser.APPROVE_OPTION) {
                            //get the file path
                            switch(fieldType){
                                case TESTING:{
                                    testingPath = fileChooser.getSelectedFile().getAbsolutePath();
                                    String[] bits = testingPath.split("\\\\");
                                    String lastOne = bits[bits.length-1];
                                    Name.setText(lastOne);
                                    break;
                                }
                                case TRAINING:{
                                    trainingPath = fileChooser.getSelectedFile().getAbsolutePath();
                                    String[] bits = trainingPath.split("\\\\");
                                    String lastOne = bits[bits.length-1];
                                    Name.setText(lastOne);
                                    break;
                                }
                                case CENTROID:{
                                    trainedPath = fileChooser.getSelectedFile().getAbsolutePath();
                                    String[] bits = trainedPath.split("\\\\");
                                    String lastOne = bits[bits.length-1];
                                    Name.setText(lastOne);
                                    break;
                                }
                                default:{
                                    ErrorHandling.throwException("this file chooser field is not supported yet and the path was not loaded");
                                    break;
                                }
                            }
                            
                        }
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
            });
            
            //listner which will remove the item in the field when clicked
            Name.addMouseMotionListener(new MouseMotionListener(){
                @Override
                public void mouseMoved(MouseEvent  e) {
                    try{
                        Name.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
                @Override
                public void mouseDragged(MouseEvent e) {
                }
            });
        }catch(Exception ex){
            ErrorHandling.output(Level.WARNING, ex);
        }
        
        return Name;
    }
}
