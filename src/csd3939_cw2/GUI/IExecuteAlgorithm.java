package csd3939_cw2.GUI;

import csd3939_cw2.DataTypes.AlgorithmType;

/**
 * interface used to bridge GUI with the algorithm implementation
 * @author Mark
 */
public interface IExecuteAlgorithm {
    public void test (AlgorithmType algorithmType, String testPath, String trainingPath, int kValue);
    public void test (AlgorithmType algorithmType, String testPath, String trainingPath, String trainedPath, int kValue);
    public void test(AlgorithmType algorithmType, int kValue, String testPath, String centroidPath);
    public void train (AlgorithmType algorithmType, String trainingPath, String centroidPath, int kValue);
    
}
