/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2;

import csd3939_cw2.GUI.ConsoleOutput;
import csd3939_cw2.Utils.Config;
import java.io.IOException;

/**
 *
 * @author Mark
 */
public class CSD3939_CW2 {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Config.Init("./config.properties");
        
//        KernelK_NNOptimizedTraining k = new KernelK_NNOptimizedTraining(Files.readImageFromTextFile("./Data Set/Data Folder/PreProcessed/optdigits.tra"), 200);
//        k.execute();
        
//        KernelK_NNOptimizedTesting k = new KernelK_NNOptimizedTesting(
//                Files.readImageFromTextFile("./Data Set/Data Folder/PreProcessed/optdigits.tes"),
//                Files.readImageFromTextFile("./Data Set/Data Folder/PreProcessed/optdigits.tra"),
//                Files.readKNNMapFromTextFile("./Data Set/Data Folder/PreProcessed/KNNMap/2016-02-01_13-00-56_k200.txt"),
//                25
//        );
//        k.execute();
        
//        Kernel_K_NN k = new  Kernel_K_NN(
//                Files.readImageFromTextFile("./Data Set/Data Folder/PreProcessed/optdigits.tes"), 
//                Files.readImageFromTextFile("./Data Set/Data Folder/PreProcessed/optdigits.tra"), 
//                25
//        );
//        k.execute();
//        k.print();
        new ConsoleOutput();
    }
    
}
