/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Utils;

import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Result {
    private ArrayList<Integer> totalPerDigit;
    private ArrayList<Integer> correctPerDigit;
    private ArrayList<Double> resultsPerDigit;
    private int total;
    private int correct;
    private int numberOfValues;
    private Profiler profiler;
    private boolean started;
    public enum OutputResolution{SECONDS, MILLISECONDS};
    
    public Result(int numberOfValues) {
        this.totalPerDigit = new ArrayList<Integer>();
        this.correctPerDigit = new ArrayList<Integer>();
        this.resultsPerDigit = new ArrayList<Double>();
        
        this.profiler = new Profiler(false);
        this.started = false;
        
        this.total = 0;
        this.correct = 0;
        this.numberOfValues = numberOfValues;
        
        populateList(this.totalPerDigit, 0);
        populateList(this.correctPerDigit, 0);
    }
    
    /**
     * Start the Timer
     */
    public void startTimer(){
        this.profiler.start();
        this.started = true;
    }
    
    /**
     * increment the number of correct values per digit
     * @param digit digit to increment
     */
    public void incrementCorrectDigit(int digit){
        this.correct++;
        this.correctPerDigit.set(digit, this.correctPerDigit.get(digit)+1);
    }
    
    /**
     * increment the total value per digit
     * @param digit
     */
    public void incrementTotalDigit(int digit){
        this.total++;
        this.totalPerDigit.set(digit, this.totalPerDigit.get(digit)+1);
    }
    
    /**
     * getList the list of results
     * @return
     */
    public ArrayList<Double> getList(){
        calculateResult();
        return this.resultsPerDigit;
    }
    
    /**
     * getList the global accuracy
     * @return
     */
    public double getGlobal(){
        double output = 0.0;
        try{
            output =  calculateAccuracy(this.correct, this.total);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return output;
    }
    
    /**
     * print the results
     * @param resolution
     */
    public void print(OutputResolution resolution){
        try{
            //print timing
            switch(resolution){
                case MILLISECONDS:{
                    this.profiler.printDiffMS();
                    break;
                }
                default:{
                    this.profiler.printDiffSeconds();
                    break;
                }
            }
            
            if(!this.started){
                ErrorHandling.throwException("The timer was not started and the calculated timing is probably incorrect!");
            }
            
            //calculate result
            calculateResult();
            
            //print result
            int digitIndex = 0;
            for(Double result: resultsPerDigit){
                System.out.println(String.format(
                        "Digit: %s%d%s -> %s%f%s%% (%s%.2f%s%%)",
                        "\u001B[34m", digitIndex, "\u001B[0m",
                        "\u001B[34m", result, "\u001B[0m",
                        "\u001B[31m", result, "\u001B[0m"
                )
                );
                digitIndex++;
            }
            
            System.out.println(String.format(
                    "Global: %s%f%s%% (%s%.2f%s%%)",
                    "\u001B[34m", getGlobal(), "\u001B[0m",
                    "\u001B[32m", getGlobal(), "\u001B[0m"
            )
            );
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * Method used to populate a list with the set values
     * @param list list to populate
     * @param value value to populate with
     */
    private void populateList(ArrayList<Integer> list, int value){
        for(int i=0;i<this.numberOfValues;i++){
            list.add(value);
        }
    }
    
    /**
     * Method used to calculate the percentage accuracy
     * @param correct
     * @param total
     * @return
     */
    private double calculateAccuracy(int correct, int total){
        double accuracy = 0.0;
        try{
            accuracy = (((double)correct)/total)*100;
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return accuracy;
    }
    
    /**
     * calculate and populate
     */
    private void calculateResult(){
        try{
            for(int i=0;i<this.numberOfValues;i++){
                this.resultsPerDigit.add(
                        calculateAccuracy(
                                this.correctPerDigit.get(i),
                                this.totalPerDigit.get(i)
                        )
                );
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
