/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Utils;

import csd3939_cw2.Algorithms.KMeans.Centroid;
import csd3939_cw2.Algorithms.KNN.Accuracy;
import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.DataTypes.ImagePoint;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Files {
    
    public enum CentroidDumpType{UN_PROCESSED, PROCESSED};
    private static String SEPARATOR = "*****************************";
    
    /**
     * This method is used to load the points from the given path.
     * @param path
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static ArrayList<Image> readImageFromTextFile(String path) throws IOException{
        
        //array used to hold the maze points
        ArrayList<Image> images = new ArrayList<Image>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            int lineIndex = 1;
            
            while (line != null) {
                Image image = new Image();
                
                String[] parts = line.split(",");
                
                if(parts.length > 1){
                    if(parts.length != 65 ){
                        ErrorHandling.throwException(String.format("\nPath: %s\nError: Line %d does not have 65 elements", path, lineIndex));
                    }
                    
                    for(int i=0;i<64;i++){
                        image.addPoint(new ImagePoint(Double.parseDouble(parts[i])));
                    }
                    
                    image.setDigit(Integer.parseInt(parts[64]));
                    
                    images.add(image);
                    lineIndex++;
                }else{
                    ErrorHandling.notifyError(String.format("Blank line was skipped in file %s", path));
                }
                line = br.readLine();
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return images;
    }
    
    /**
     * This method is used to load the points from the given path.
     * @param path path to the centroid file
     * @param kValue number of centroids to be loaded
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static ArrayList<Centroid> readCentroidFromTextFile(String path, int kValue) throws IOException{
        
        //array used to hold the maze points
        ArrayList<Centroid> centroids = new ArrayList<Centroid>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            int lineIndex = 1;
            
            while (line != null) {
                Centroid centroid = new Centroid();
                
                if(line.contains(SEPARATOR)){
                    break;
                }
                String[] parts = line.split(",");
                
                if(parts.length > 1){
                    if(parts.length != 65 ){
                        ErrorHandling.throwException(String.format("\nPath: %s\nError: Line %d does not have 65 elements", path, lineIndex));
                    }
                    
                    for(int i=0;i<64;i++){
                        centroid.addPoint(new ImagePoint(Double.parseDouble(parts[i])));
                    }
                    
                    centroid.setDigit(Integer.parseInt(parts[64]));
                    
                    centroids.add(centroid);
                    lineIndex++;
                }else{
                    ErrorHandling.notifyError(String.format("Blank line was skipped in file %s", path));
                }
                line = br.readLine();
            }
            
            //make sure that k number of centroids are loaded
            if(centroids.size() > kValue){
                ErrorHandling.notifyError(
                        String.format(
                                "A maximum of %d centroids where expected yet %d where found! Make sure you are using the correct dataset",
                                kValue, centroids.size()
                        )
                );
            }
        } catch(Exception ex){
            centroids = null;
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return centroids;
    }
    
    /**
     * This method is used to load the accuracy map from the given path.
     * @param path path to the centroid file
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static ArrayList<Accuracy> readKNNMapFromTextFile(String path) throws IOException{
        
        //array used to hold the maze points
        ArrayList<Accuracy> accuracyList = new ArrayList<Accuracy>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            int lineIndex = 1;
            
            while (line != null) {
                String[] parts = line.split(",");
                
                if(parts.length > 1){
                    if(parts.length != 3 ){
                        ErrorHandling.throwException(String.format("\nPath: %s\nError: Line %d does not have 3 elements", path, lineIndex));
                    }
                    accuracyList.add(
                            new Accuracy(
                                    Integer.parseInt(parts[0]),
                                    Integer.parseInt(parts[1]),
                                    Double.parseDouble(parts[2])
                            )
                    );
                    
                    lineIndex++;
                }else{
                    ErrorHandling.notifyError(String.format("Blank line was skipped in file %s", path));
                }
                line = br.readLine();
            }
            
            
        } catch(Exception ex){
            accuracyList = null;
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return accuracyList;
    }
    
    /**
     * This method is used to load the points from the given path.
     * @param centroidList
     * @param accuracy
     * @param type
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String dumpCentroidToTextFile(ArrayList<Centroid> centroidList, double accuracy, AlgorithmType algorithmType, CentroidDumpType type) throws IOException{
        BufferedWriter output = null;
        String fileName = "";
        try {
            if(centroidList != null){
                if(!centroidList.isEmpty()){
                    fileName = String.format("%s/%s_%s_%s_%.2f%%.txt",
                            Config.getPropertyString("centroidFileChooserDefaultDirectory"),
                            type.toString(),
                            new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()),
                            algorithmType.toString(),
                            accuracy
                    );
                    
                    File file = new File(fileName);
                    output = new BufferedWriter(new FileWriter(file));
                    
                    for(Centroid c : centroidList){
                        output.write(c.toString() +"\n");
                    }
                    
                    //print the accuracies
                    output.write(SEPARATOR + "\n");
                    if(type == CentroidDumpType.PROCESSED){
                        for(Centroid c : centroidList){
                            output.write(String.format("%d => %.2f\n", c.getDigit() ,c.getAccuracy()));
                        }
                    }
                    output.write(SEPARATOR);
                }else{
                    ErrorHandling.throwException("Cannot dump centoirds to list as centroidList is empty");
                }
            }else{
                ErrorHandling.throwException("Cannot dump centoirds to list as centroidList is Null");
            }
        } catch ( Exception ex ) {
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( output != null ) output.close();
        }
        return fileName;
    }
    
    /**
     * This method is used to load the points from the given path.
     * @param accuracyList
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static String dumpKNNMapToTextFile(ArrayList<Accuracy> accuracyList, AlgorithmType type, int nValue) throws IOException{
        BufferedWriter output = null;
        String fileName = "";
        try {
            if(accuracyList != null){
                if(!accuracyList.isEmpty()){
                    fileName = String.format("%s/%s_%s_k%d.txt",
                            Config.getPropertyString("knnMapFileChooserDefaultDirectory"),
                            new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()),
                            type.toString(),
                            nValue
                    );
                    
                    File file = new File(fileName);
                    output = new BufferedWriter(new FileWriter(file));
                    
                    for(Accuracy a : accuracyList){
                        output.write(a.toString() +"\n");
                    }
                    
                }else{
                    ErrorHandling.throwException("Cannot dump centoirds to list as centroidList is empty");
                }
            }else{
                ErrorHandling.throwException("Cannot dump centoirds to list as centroidList is Null");
            }
        } catch ( Exception ex ) {
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( output != null ) output.close();
        }
        return fileName;
    }
}
