/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Utils;

import csd3939_cw2.DataTypes.Image;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Static class which holds some mathematical algorithms
 * @author Mark
 */
public class Calcs {
    
    /**
     * Calculate the RMS Distance between the points of two images
     * @param testImage
     * @param traImage
     * @return 
     */
    public static int RMS(Image testImage, Image traImage){
        int returnValue = 0;
        try{
            if(testImage.getNumberOfPoints() == traImage.getNumberOfPoints()){
                for(int i = 0;i<testImage.getNumberOfPoints();i++){
                    returnValue += Math.pow(testImage.getValue(i) - traImage.getValue(i), 2);
                }
            }else{
                ErrorHandling.throwException("The testing and training images do not have the same numebr of points");
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return returnValue;
    }
    
}
