/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KNN;

import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.ErrorHandling;
import static csd3939_cw2.Utils.Files.dumpKNNMapToTextFile;
import csd3939_cw2.Utils.Profiler;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class K_NNOptimizedTraining {
    private ArrayList<Image> trainingSetOne;
    private ArrayList<Image> trainingSetTwo;
    private ArrayList<Accuracy> accuracy;
    private int nValue;
    
    public K_NNOptimizedTraining(ArrayList<Image> trainingSet, int nValue) {
        try{
            if(trainingSet != null){
                //split the set into two fold
                int trainingSetSize = trainingSet.size();
                int halfTrainingSetSize = trainingSetSize/2;
                this.trainingSetOne = new ArrayList<Image>(trainingSet.subList(0, halfTrainingSetSize));
                this.trainingSetTwo = new ArrayList<Image>(trainingSet.subList(halfTrainingSetSize, trainingSetSize));
                
                //create blank accuracy list
                this.accuracy = new ArrayList<Accuracy>();
                for(int i=0;i<10;i++){
                    this.accuracy.add(new Accuracy(i));
                }
                
                this.nValue = nValue;
            }else{
                ErrorHandling.throwException("trainingSet passed was null!");
            }
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * Method used to processImage this algorithm
     */
    public void execute(){
        try{
            Profiler profiler = new Profiler(true);
            ArrayList<ArrayList<Double>> resultsOneTwo = getKNNResults(this.trainingSetOne, this.trainingSetTwo, this.nValue);
            
            //loop the result list
            int kIndex = 1;
            for(ArrayList<Double> result : resultsOneTwo){
                //loop every digit of every result
                int digitIndex = 0;
                for(Double digitAccuracy: result){
                    //get the accuracy of every digit and update the accuracy and kvalue
                    //the update method only updates if the accuracy passed is bigger then that currently set in the accuracy object
                    this.accuracy.get(digitIndex).updateAccuracy(kIndex, digitAccuracy);
                    digitIndex++;
                }
                kIndex++;
            }
            profiler.printDiffSeconds();
            String fileName = dumpKNNMapToTextFile(this.accuracy, AlgorithmType.K_NNOptimized, this.nValue);
            System.out.println(String.format("File saved: %s%s%s", "\u001B[34m", fileName, "\u001B[0m"));
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
    }
    
    /**
     * get the results from the KNN algorithm
     * @param setA
     * @param setB
     * @param nValue
     * @return
     */
    private ArrayList<ArrayList<Double>> getKNNResults(ArrayList<Image> setA, ArrayList<Image> setB, int nValue){
        ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
        
        //loop for n times
        for(int i=1;i<nValue+1;i++){
            
            //perfom the knn algorithm with k=i, k->n for the sets passed
            K_NN k_nn = new K_NN(setA, setB, i);
            k_nn.execute();
            
            //add the results to the results list
            results.add(k_nn.getResult().getList());
            
            //print % status
            double readyPerC = (((double)i)/nValue)*100;
            System.out.println(String.format("%.2f%%...", readyPerC));
        }
        return results;
    }
}
