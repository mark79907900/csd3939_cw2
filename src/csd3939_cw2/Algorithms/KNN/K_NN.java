/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KNN;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Calcs;
import csd3939_cw2.Utils.Result;
import csd3939_cw2.Utils.Result.OutputResolution;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

/**
 * K-Nearest neighbour algorithm used to detect the characters and asses the
 * accuracy
 * @author Mark
 */
public class K_NN {
    private final ArrayList<Image> testingSet;
    private final ArrayList<Image> trainingSet;
    private int kValue;
    private Result result;
    
    public K_NN(ArrayList<Image> testingSet, ArrayList<Image> trainingSet, int kValue) {
        this.testingSet = testingSet;
        this.trainingSet = trainingSet;
        this.kValue = kValue;
        this.result = new Result(10);
    }
    
    /**
     * Method used to execute the algorithm for the whole testing set
     * @param kValue
     */
    public void execute(int kValue) {
        int testingIndex = 0;
        
        this.result.startTimer();
        for (Image testing : this.testingSet) {
            int trainingDigit = processImage(testing, kValue);
            
            //asses the accuracy of the algorithm
            //get the value of the character from testing set by using the indexes
            int testingDigit = this.testingSet.get(testingIndex).getDigit();
            
            //if the values of from the testing and training sets are equal add to the correct counter
            if(trainingDigit == testingDigit){
                this.result.incrementCorrectDigit(testingDigit);
            }
            this.result.incrementTotalDigit(testingDigit);
            testingIndex++;
        }
    }
    
    /**
     * Method used to execute the algorithm for the whole testing set
     */
    public void execute(){
        execute(this.kValue);
    }
    
    /**
     * Get the results object
     * @return 
     */
    public Result getResult(){
        return this.result;
    }
    /**
     * print the output
     */
    public void print(){
        this.result.print(OutputResolution.SECONDS);
    }
    
    /**
     * This method is used to perform K-NN on a single image
     * @param testing
     * @param kValue
     * @return
     */
    public int processImage(Image testing, int kValue){
        int trainingIndex = 0;
        ArrayList<ArrayList<Integer>> rmsList = new ArrayList<ArrayList<Integer>>();
        
        for (Image training : this.trainingSet) {
            int rmsValue = Calcs.RMS(testing, training);
            
            //get the character from the training set
            int trainingCharacter = this.trainingSet.get(trainingIndex).getDigit();
            
            //add the chracter with its rms value to the map
            rmsList.add(new ArrayList<Integer>(Arrays.asList(rmsValue, trainingCharacter)));
            trainingIndex++;
        }
        
        //sort the list, smallest RMS First
        sort(0, rmsList);
        
        //if kValue is greater then the list size, override the kvalue with listsize
        if(kValue>rmsList.size()){
            kValue = rmsList.size();
        }
        
        //create a sublist with the first K values
        List<ArrayList<Integer>> kList = rmsList.subList(0, kValue);
        
        //create an occurrenceMap and find the occurrences of the characters
        HashMap occurrenceMap = new HashMap();
        for(ArrayList<Integer> item : kList){
            int digit = item.get(1);
            if(occurrenceMap.containsKey(digit)){
                occurrenceMap.put(digit, (int)occurrenceMap.get(digit)+1);
            }else{
                occurrenceMap.put(digit, 1);
            }
        }
        
        //Find the key(digit) with the biggest occurance
        return (int)Collections.max(occurrenceMap.entrySet(),
                new Comparator<Entry<Integer,Integer>>(){
                    @Override
                    public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
                        return o1.getValue() > o2.getValue()? 1:-1;
                    }
                }).getKey();
    }
    
    /**
     * method used to sort a multidimensional arraylist of Integer
     * @param columnIndex column which will be used for sorting
     * @param list list which will be sorted
     */
    private void sort(int columnIndex, ArrayList<ArrayList<Integer>> list) {
        Comparator<ArrayList<Integer>> myComparator = new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
                return o1.get(columnIndex).compareTo(o2.get(columnIndex));
            }
        };
        Collections.sort(list, myComparator);
    }
    
}
