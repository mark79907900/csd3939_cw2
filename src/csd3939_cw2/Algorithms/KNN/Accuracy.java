/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csd3939_cw2.Algorithms.KNN;

/**
 *
 * @author Mark
 */
public class Accuracy {
    private int digit;
    private int kValue;
    private double accuracy;

    public Accuracy(int digit) {
        this.digit = digit;
        this.kValue = 1;
        this.accuracy = 0.0;
    }

    public Accuracy(int digit, int kValue, double accuracy) {
        this.digit = digit;
        this.kValue = kValue;
        this.accuracy = accuracy;
    }
    
    /**
     * update the accuracy and kValue if the accuracy is bigger then the one currently set 
     * @param kValue
     * @param accuracy 
     */
    public void updateAccuracy(int kValue, double accuracy){
        if(this.accuracy < accuracy){
            this.accuracy = accuracy;
            this.kValue = kValue;
        }
    }
    
    /**
     * get the kValue
     * @return 
     */
    public int getKValue(){
        return this.kValue;
    }
    
    @Override
    public String toString(){
        return String.format("%d,%d,%f", this.digit, this.kValue, this.accuracy);
    }
    
}
