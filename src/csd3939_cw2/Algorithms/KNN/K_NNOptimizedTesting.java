/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KNN;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Result;
import csd3939_cw2.Utils.Result.OutputResolution;
import java.util.ArrayList;

/**
 *
 * @author Mark
 */
public class K_NNOptimizedTesting {
    private ArrayList<Image> testingSet;
    private ArrayList<Image> trainingSet;
    private ArrayList<Accuracy> accuracy;
    private int nValue;
    private Result result;
    
    
    public K_NNOptimizedTesting(ArrayList<Image> testingSet, ArrayList<Image> trainingSet, ArrayList<Accuracy> accuracy, int nValue) {
        this.testingSet = testingSet;
        this.trainingSet = trainingSet;
        this.accuracy = accuracy;
        this.nValue = nValue;
        this.result = new Result(10);
    }
    
    public void execute(){
        this.result.startTimer();
        
        //get knn object
        K_NN k_nn = new K_NN(this.testingSet, this.trainingSet, 1);
        
        //loop the testset
        for(Image testImage : testingSet){
            int counter = 1;
            int testDigit = -1;
            
            //loop until a testDigit is found, if it is not found loop for nTimes
            while(testDigit < 0 && counter < this.nValue){
                testDigit = testKNN(k_nn, testImage, counter);
                counter++;
            }
            
            //if it is still not found use knn for k=1
            if(testDigit == -1){
                testDigit = k_nn.processImage(testImage, 1);
            }
            
            //test results
            if(testImage.getDigit() == testDigit){
                this.result.incrementCorrectDigit(testImage.getDigit());
            }
            this.result.incrementTotalDigit(testImage.getDigit());
        }
        this.result.print(OutputResolution.SECONDS);
    }
    
    /**
     * Method used to test a single knn. It tests for k=kValue, gets the ideal k-value from the accuracy map and re-tests for that k-value
     * Both digits are compared, if they are equal the digit is returned, if they are not -1 is returned
     * @param k_nn
     * @param testImage
     * @param kValue
     * @return 
     */
    private int testKNN(K_NN k_nn, Image testImage, int kValue){
        int digit = k_nn.processImage(testImage, kValue);
        int kValueTest = this.accuracy.get(digit).getKValue();
        int testDigit = k_nn.processImage(testImage, kValueTest);
        
        if(digit != testDigit){
            return -1;
        }
        return testDigit;
    }
}
