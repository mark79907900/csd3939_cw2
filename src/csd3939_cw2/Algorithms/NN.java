/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Calcs;
import csd3939_cw2.Utils.Result;
import csd3939_cw2.Utils.Result.OutputResolution;
import java.util.ArrayList;

/**
 * Nearest neighbour algorithm used to detect the characters and asses the accuracy
 * @author Mark
 */
public class NN {
    private final ArrayList<Image> testingSet;
    private final ArrayList<Image> trainingSet;
    private int trainingIndex;
    private int testingIndex;
    
    private Result result;
    
    public NN(ArrayList<Image> testingSet, ArrayList<Image> trainingSet) {
        this.testingSet = testingSet;
        this.trainingSet = trainingSet;
        this.result = new Result(10);
    }
    
    /**
     * Method used to execute the algorithm
     */
    public void execute(){
        int testingIndex = 0;
        
        this.result.startTimer();
        for(Image testing : this.testingSet){
            
            int trainingIndex = 0;
            int min = Integer.MAX_VALUE;
            
            for(Image training : this.trainingSet){
                int rmsValue = Calcs.RMS(testing, training);
                
                //find the lowest RMS value and keep the index of both training and testing set
                if(min > rmsValue){
                    min = rmsValue;
                    this.trainingIndex = trainingIndex;
                    this.testingIndex = testingIndex;
                }
                trainingIndex++;
            }
            
            //asses the accuracy of the algorithm
            
            //get the values from both the testing and traing sets by using the indexes
            int trainingCharacter = this.trainingSet.get(this.trainingIndex).getDigit();
            int testingCharacter = this.testingSet.get(this.testingIndex).getDigit();
            
            //if the values of from the testing and training sets are equal add to the correct counter
            if(trainingCharacter == testingCharacter){
                this.result.incrementCorrectDigit(testingCharacter);
            }
            this.result.incrementTotalDigit(testingCharacter);
            testingIndex++;
        }
        this.result.print(OutputResolution.SECONDS);
    }
    
}
