/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KMeans;

import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Config;
import csd3939_cw2.Utils.ErrorHandling;
import csd3939_cw2.Utils.Files;
import csd3939_cw2.Utils.Profiler;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class KMeansTraning extends KMeans{
    private final ArrayList<Image> trainingSet;
    private ArrayList<Centroid> centroidSet;
    private final int kValue;
    
    public KMeansTraning(ArrayList<Image> trainingSet, ArrayList<Centroid> centroidSet, int kValue) {
        this.trainingSet = trainingSet;
        this.centroidSet = centroidSet;
        this.kValue = kValue;
    }
    
    /**
     * Method used to execute the algorithm
     */
    public void execute() {
        double accuracy = -1;
        
        try{
            ArrayList<Centroid> unprocessed = null;
            boolean outputUnprocessed = false;
            
            //get the minimum accuracy threshold
            int minimumAccuracyThreshold = Config.getPropertyInt("minimumAccuracyThreshold");
            Profiler profiler = new Profiler(true);
            while(accuracy < minimumAccuracyThreshold){
                
                //if the list of centroid is null, generate a new list of centroids
                if(this.centroidSet == null){
                    this.centroidSet = new ArrayList<Centroid>();
                    for(int i=0;i<this.kValue;i++){
                        switch(Config.getPropertyInt("randomCentroid")){
                            case 1:{
                                this.centroidSet.add(new Centroid(true));
                                break;
                            }
                            default:{
                                this.centroidSet.add(new Centroid(this.trainingSet));
                                break;
                            }
                        }
                    }
                    unprocessed = new ArrayList<Centroid>();
                    for(Centroid centroid : this.centroidSet){
                        unprocessed.add(centroid.clone());
                    }
                    outputUnprocessed = true;
                }
                
                //loop until shifts are equal to the number set in the config file
                //loop until convergance
                int shifts = Integer.MAX_VALUE;
                while(shifts > Config.getPropertyInt("minShiftsThreshold")){
                    //attach the images to the closest centroid
                    attachImagesToCentroids(this.centroidSet, this.trainingSet);
                    
                    shifts = recalculateCentroids(this.centroidSet);
//                    System.out.println(String.format("Shifts: %d",shifts));
                }
                
                //choose which classifying algorithm to use
                switch(Config.getPropertyInt("classifierVersion")){
                    case 1:{
                        accuracy = classifyCentroidsV1(this.centroidSet);
                        break;
                    }
                    case 2:{
                        accuracy = classifyCentroidsV2(this.centroidSet);
                        break;
                    }
                    default:{
                        ErrorHandling.throwException(
                                "The Classifier version selected in the config file does not exist!"
                                        + "\nchange the version of the attribute 'classifierVersion' to fix this problem"
                        );
                        break;
                    }
                }
                
                if(accuracy < minimumAccuracyThreshold ){
                    this.centroidSet = null;
                    unprocessed = null;
                    outputUnprocessed = false;
                }
            }
            profiler.printDiffSeconds();
            if(outputUnprocessed){
                String fileName = Files.dumpCentroidToTextFile(unprocessed, accuracy, AlgorithmType.K_Means, Files.CentroidDumpType.UN_PROCESSED);
                System.out.println(String.format("File saved: %s%s%s", "\u001B[34m", fileName, "\u001B[0m"));
            }
            
            sortCentroidForPrinting(this.centroidSet);
            String fileName = Files.dumpCentroidToTextFile(this.centroidSet, accuracy, AlgorithmType.K_Means, Files.CentroidDumpType.PROCESSED);
            System.out.println(String.format("File saved: %s%s%s", "\u001B[34m", fileName, "\u001B[0m"));
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
}
