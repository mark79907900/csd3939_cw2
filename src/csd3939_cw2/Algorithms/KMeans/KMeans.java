/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KMeans;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Calcs;
import csd3939_cw2.Utils.Config;
import csd3939_cw2.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * K-Means neighbour algorithm used to detect the characters and asses the
 * accuracy
 * @author Mark
 */
public abstract class KMeans {
    
    /**
     * find the nearest centroid from the list to the image passed
     * @param centroidSet
     * @param image
     * @return 
     */
    public Centroid findNearestCentroid(ArrayList<Centroid> centroidSet, Image image){
        //create a list to hold the rms values for every training image
        HashMap rmsCentroidsList = new HashMap();
        
        //get the rms values for all the centroids and add it to the list
        int index = 0;
        for(Centroid centroid : centroidSet){
            rmsCentroidsList.put(index, Calcs.RMS(centroid, image));
            index++;
        }
        
        //sort the list, smallest rms first
        Stream<Map.Entry<Integer,Double>> sortedRmsCentroidsList = rmsCentroidsList.entrySet().stream().sorted(Map.Entry.comparingByValue());
        
        //get the closest centroid Entry(key value parir) from the list
        Map.Entry closestCentroid = (Map.Entry)sortedRmsCentroidsList.toArray()[0];
        
        //get the acutal centroid object
        return centroidSet.get((int)closestCentroid.getKey());
    }
    
    /**
     *check the euclidian distance of every training image and centroids and attach the training point to the nearest centroid
     * @param centroidSet
     * @param trainingSet
     */
    protected void attachImagesToCentroids(ArrayList<Centroid> centroidSet, ArrayList<Image> trainingSet){
        try{
            for(Image image: trainingSet){
                Centroid centroid = findNearestCentroid(centroidSet, image);
                centroid.attachImage(image);
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * re-calculate the mean values of the centroid and find the number of changes that took place
     * @param centroidSet
     */
    protected int recalculateCentroids(ArrayList<Centroid> centroidSet){
        int totalShifts = 0;
        for(Centroid centroid : centroidSet){
            centroid.reCalculate();
            totalShifts = centroid.changes();
            centroid.cacheAttachedImageList();
            centroid.clearAttachedImages();
        }
        return totalShifts;
    }
    
    /**
     * Classify the centroids in their respective digits
     * @param centroidSet
     * @return
     */
    protected double classifyCentroidsV1(ArrayList<Centroid> centroidSet){
        double minAccuacy = 100;
        
        for(Centroid centroid : centroidSet){
            double accuracy = centroid.classify();
            if(minAccuacy > accuracy){
                minAccuacy = accuracy;
            }
//            System.out.println(centroid.getDigit() + " - " + accuracy);
        }
        
        //make sure that there is at least one of each digit
        HashMap occurrenceMap = new HashMap();
        for(Centroid centroid : centroidSet){
            int digit = centroid.getDigit();
            if(occurrenceMap.containsKey(digit)){
                occurrenceMap.put(digit, (int)occurrenceMap.get(digit)+1);
            }else{
                occurrenceMap.put(digit, 1);
            }
        }
        
        //make a list of items which should be found in the list
        Set digits = Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).collect(Collectors.toCollection(HashSet::new));
        
        //remove the occurrences from the digits list, this means that if a digit was not removed, it was not in the list of occurrences
        digits.removeAll(occurrenceMap.keySet());
        
        //check that all the digits where found
        if(digits.size() > 0){
            minAccuacy = -1;
        }
        
        System.out.println("Minimum accuracy: " + minAccuacy);
        return minAccuacy;
    }
    
    /**
     * Classify the centroids in their respective digits
     * This classifier removes all the centroids which are below the minimum accuracy threshold
     * @param centroidSet
     * @return
     */
    protected double classifyCentroidsV2(ArrayList<Centroid> centroidSet){
        double minAccuacy = 100;
        
        ArrayList<Centroid> toRemove = new ArrayList<Centroid>();
        
        for(Centroid centroid : centroidSet){
            double accuracy = centroid.classify();
            if(accuracy < Config.getPropertyInt("minimumAccuracyThreshold")){
                toRemove.add(centroid);
            }
            if(minAccuacy > accuracy && accuracy > Config.getPropertyInt("minimumAccuracyThreshold")){
                minAccuacy = accuracy;
            }
//            System.out.println(centroid.getDigit() + " - " + accuracy);
        }
        
        //remove centroids
        centroidSet.removeAll(toRemove);
        
        //make sure that there is at least one of each digit
        HashMap occurrenceMap = new HashMap();
        for(Centroid centroid : centroidSet){
            int digit = centroid.getDigit();
            if(occurrenceMap.containsKey(digit)){
                occurrenceMap.put(digit, (int)occurrenceMap.get(digit)+1);
            }else{
                occurrenceMap.put(digit, 1);
            }
        }
        
        //make a list of items which should be found in the list
        Set digits = Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).collect(Collectors.toCollection(HashSet::new));
        
        //remove the occurrences from the digits list, this means that if a digit was not removed, it was not in the list of occurrences
        digits.removeAll(occurrenceMap.keySet());
        
        //check that all the digits where found
        if(digits.size() > 0){
            minAccuacy = -1;
        }
        
        System.out.println("minAccuacy " + minAccuacy);
        return minAccuacy;
    }
    
    /**
     * This method is used to sort the centroid list based on the digit smallest first
     * @param centroidSet
     */
    protected void sortCentroidForPrinting(ArrayList<Centroid> centroidSet){
        Collections.sort(centroidSet, new Comparator<Centroid>() {
            @Override
            public int compare(Centroid c1, Centroid c2) {
                return Integer.compare(c1.getDigit(), c2.getDigit());
            }
            
        });
    }
    
}
