/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KMeans;

import csd3939_cw2.DataTypes.AlgorithmType;
import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.ErrorHandling;
import csd3939_cw2.Utils.Files;
import csd3939_cw2.Utils.Profiler;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class K_MeansTraningOptimised extends KMeans{
    private final ArrayList<Image> trainingSet;
    private ArrayList<Centroid> centroidSet;
    private final int kValue;
    
    public K_MeansTraningOptimised(ArrayList<Image> trainingSet, ArrayList<Centroid> centroidSet, int kValue) {
        this.trainingSet = trainingSet;
        this.centroidSet = centroidSet;
        this.kValue = kValue;
    }
    
    public void execute(){
        try{
            ArrayList<Centroid> centroids = new ArrayList<Centroid>();
            
            Profiler profiler = new Profiler(true);
            //generate 10 centroids, one for each digit
            for(int i=0; i<this.kValue;i++){
                Centroid c = new Centroid();
                c.setBlank();
                c.setDigit(i);
                centroids.add(c);
            }
            
            //distribute the images based on thier digit
            for(Image traningImage : this.trainingSet){
                centroids.get(traningImage.getDigit()).attachImage(traningImage);
            }
            
            //recalculate the centroid of each cluster
            recalculateCentroids(centroids);
            
            //print the time taken
            profiler.printDiffMS();
            
            //dump the centroids to file
            String fileName = Files.dumpCentroidToTextFile(centroids, 100, AlgorithmType.K_MeansOptimised, Files.CentroidDumpType.PROCESSED);
            System.out.println(String.format("File saved: %s%s%s", "\u001B[34m", fileName, "\u001B[0m"));
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
}
