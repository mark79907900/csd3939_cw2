/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KMeans;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.ErrorHandling;
import csd3939_cw2.Utils.Result;
import csd3939_cw2.Utils.Result.OutputResolution;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class KMeansTesting extends KMeans{
    private final ArrayList<Image> testingSet;
    private final ArrayList<Centroid> centroidSet;
    private final int kValue;
    private Result result;
    
    
    public KMeansTesting(ArrayList<Image> testingSet, ArrayList<Centroid> centroidSet, int kValue) {
        this.testingSet = testingSet;
        this.centroidSet = centroidSet;
        this.kValue = kValue;
        this.result = new Result(10);
    }
    
    /**
     * Method used to execute the algorithm
     */
    public void execute() {
        try{
            this.result.startTimer();
            for(Image testImage : this.testingSet){
                int testDigit = testImage.getDigit();
                Centroid centroid = findNearestCentroid(centroidSet, testImage);
                
                if(centroid.getDigit() == testDigit){
                    this.result.incrementCorrectDigit(testDigit);
                }
                this.result.incrementTotalDigit(testDigit);
            }
            this.result.print(OutputResolution.MILLISECONDS);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
