/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms.KMeans;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.DataTypes.ImagePoint;
import csd3939_cw2.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Centroid extends Image implements Cloneable{
    private ArrayList<Image> attachedImages;
    private ArrayList<Image> oldAttachedImages;
    private double accuracy;
    
    public Centroid() {
        super();
        super.setDigit(-1);
        this.attachedImages = new ArrayList<>();
        this.oldAttachedImages = null;
        this.accuracy = 0.0;
    }
    
    /**
     * Generate a random centroid(with random points)
     * @param isRandom 
     */
    public Centroid(boolean isRandom){
        this();
        if(isRandom){
            random();
        }
    }
    
    /**
     * Pick a random image and use it as a centroid 
     * @param trainingSet 
     */
    public Centroid(ArrayList<Image> trainingSet){
        this();
        try{
            Random randomizer = new Random();
            ArrayList<ImagePoint> points = trainingSet.get(randomizer.nextInt(trainingSet.size())).getImagePoints();
            for(ImagePoint point: points){
                super.addPoint(new ImagePoint(point.getDouble()));
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
    }
    
    /**
     * randomise the points of this centroid
     */
    public void random(){
        for(int i=0;i<64;i++){
            super.addPoint(new ImagePoint());
        }
    }
    
    /**
     * Attach a new image to this centroid
     * @param image
     */
    public void attachImage(Image image){
        this.attachedImages.add(image);
    }
    
    /**
     * re-calculate the centre value of the centroid
     */
    public void reCalculate(){
        try{
            
            Image centroid = new Image();
//            centroid.setBlank();
            centroid.setImagePoints(getImagePoints());
            
            //get the total for every element
            for(Image image: this.attachedImages){
                int pointIndex = 0;
                for(ImagePoint point : image.getImagePoints()){
                    double value = centroid.getValue(pointIndex);
                    centroid.setValue(pointIndex, value + point.getDouble(), false);
                    pointIndex++;
                }
            }
            
            //devide each element by the number of images
            for(ImagePoint point : centroid.getImagePoints()){
                int numberOfImages = this.attachedImages.size()+1;
                double avg = 0.0;
                
                if(numberOfImages > 0){
                    avg = ((double)point.getDouble())/numberOfImages;
                }
                point.setValue(avg);
            }
            
            //overwrite the old points of the centroid and write new once
            super.setImagePoints(centroid.getImagePoints());
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * return the number of changes that happened between the old and new attached image lists
     * @return
     */
    public int changes(){
        int oldValue = 0;
        int newValue = 0;
        try{
            if(this.oldAttachedImages != null){
                ArrayList<Image> tempOld = new ArrayList(this.oldAttachedImages);
                tempOld.removeAll(this.attachedImages);
                oldValue = tempOld.size();
                
                ArrayList<Image> temp = new ArrayList(this.attachedImages);
                temp.removeAll(this.oldAttachedImages);
                newValue = temp.size();
            }else{
                return Integer.MAX_VALUE;
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.WARNING, ex);
        }
        
        return oldValue + newValue;
    }
    
    /**
     * Clear the image items attached to this centroid
     */
    public void clearAttachedImages(){
        this.attachedImages.clear();
    }
    
    /**
     * cache the images from the main attached image list to the old image list
     */
    public void cacheAttachedImageList(){
        this.oldAttachedImages = new ArrayList(this.attachedImages);
    }
    
    public double getAccuracy() {
        return accuracy;
    }
    
    /**
     * Classify the centroid based on majority voting
     * The most dominant digit wins
     * The precision is calculated
     * @return
     */
    public double classify(){
        int numberOfImages = this.oldAttachedImages.size();
        
        if(numberOfImages > 0){
            
            //create an occurrenceMap and find the occurrences of the digit
            HashMap occurrenceMap = new HashMap();
            for(Image image : this.oldAttachedImages){
                int digit = image.getDigit();
                if(occurrenceMap.containsKey(digit)){
                    occurrenceMap.put(digit, (int)occurrenceMap.get(digit)+1);
                }else{
                    occurrenceMap.put(digit, 1);
                }
            }
            
            //Find the key(digit) with the biggest occurance
            int keyOfMaxOccurrence = (int)Collections.max(
                    occurrenceMap.entrySet(),
                    new Comparator<Map.Entry<Integer,Integer>>(){
                        @Override
                        public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                            return o1.getValue() > o2.getValue()? 1:-1;
                        }
                    }).getKey();
            
            super.setDigit(keyOfMaxOccurrence);
            this.accuracy = (((int)occurrenceMap.get(keyOfMaxOccurrence))/(numberOfImages*1.0))*100;
            return this.accuracy;
        }
        return 0.0;
    }
    
    @Override
    public String toString(){
        String output = "";
        try{
            for(ImagePoint p : super.getImagePoints()){
                output += p.toString();
                output += ",";
            }
            output += super.getDigit();
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return output;
    }
    
    @Override
    public Centroid clone() throws CloneNotSupportedException{
        return (Centroid)super.clone();
    }
}
