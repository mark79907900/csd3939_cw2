/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package csd3939_cw2.Algorithms;

import csd3939_cw2.DataTypes.Image;
import csd3939_cw2.Utils.Calcs;
import csd3939_cw2.Utils.Config;
import csd3939_cw2.Utils.ErrorHandling;
import csd3939_cw2.Utils.Result;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

/**
 * K-Nearest neighbour algorithm used to detect the characters and asses the
 * accuracy
 * @author Mark
 */
public class WeightedParzenWindows {
    private final ArrayList<Image> testingSet;
    private final ArrayList<Image> trainingSet;
    private final int rmsThreshold;
    private Result result;
    private double rmsThresholdIsTooSmall;
    
    public WeightedParzenWindows(ArrayList<Image> testingSet, ArrayList<Image> trainingSet, int rmsThreshold) {
        this.testingSet = testingSet;
        this.trainingSet = trainingSet;
        this.rmsThreshold = rmsThreshold;
        this.result = new Result(10);
        this.rmsThresholdIsTooSmall = Double.MAX_VALUE;
    }
    
    /**
     * Method used to execute the algorithm for the whole testing set
     * @param rmsThreshold
     */
    public void execute(int rmsThreshold) {
        int testingIndex = 0;
        
        this.result.startTimer();
        for (Image testing : this.testingSet) {
            int trainingDigit = processImage(testing, this.rmsThreshold);
            
            //asses the accuracy of the algorithm
            //get the value of the character from testing set by using the indexes
            int testingDigit = this.testingSet.get(testingIndex).getDigit();
            
            //if the values of from the testing and training sets are equal add to the correct counter
            if(trainingDigit == testingDigit){
                this.result.incrementCorrectDigit(testingDigit);
            }
            this.result.incrementTotalDigit(testingDigit);
            testingIndex++;
        }
    }
    
    /**
     * Method used to execute the algorithm for the whole testing set
     */
    public void execute(){
        execute(this.rmsThreshold);
    }
    
    /**
     * Get the results object
     * @return
     */
    public Result getResult(){
        return this.result;
    }
    /**
     * print the output
     */
    public void print(){
        if(this.rmsThresholdIsTooSmall < Double.MAX_VALUE){
            System.out.println(
                    String.format("%s%s%s%d%s%s",
                            "\u001B[31m", "The RMS Threshold is to small, set a value greater then or equal to <",
                            "\u001B[35m", (int)this.rmsThresholdIsTooSmall,
                            "\u001B[31m",">",
                            "\u001B[0m"
                    )
            );
            this.rmsThresholdIsTooSmall = -1;
        }
        this.result.print(Result.OutputResolution.SECONDS);
    }
    
    /**
     * This method is used to perform ParzenWindows on a single test image
     * returns -1 if the rmsThreshold is too small
     * @param testing
     * @param rmsThreshold
     * @return
     */
    public int processImage(Image testing, int rmsThreshold){
        try{
            int trainingIndex = 0;
            ArrayList<ArrayList<Integer>> rmsList = new ArrayList<ArrayList<Integer>>();
            
            for (Image training : this.trainingSet) {
                int rmsValue = Calcs.RMS(testing, training);
                
                //get the character from the training set
                int trainingCharacter = this.trainingSet.get(trainingIndex).getDigit();
                
                //add the chracter with its rms value to the map
                rmsList.add(new ArrayList<Integer>(Arrays.asList(rmsValue, trainingCharacter)));
                trainingIndex++;
            }
            
            //sort the list, smallest RMS First
            sort(0, rmsList);
            
            //create the kList with the kernel function
            List<ArrayList<Double>> areaList = null;
            if(Config.getPropertyInt("weightedOFFPWVersion") == 0){
                areaList = binary(rmsList, rmsThreshold);
            }
            else{
                areaList = weighted(rmsList, rmsThreshold);
            }
            
            //check if the threshold set is too small
            if(areaList.size() > 0){
                //create an occurrenceMap and find the occurrences of the digit, update the weight for each
                HashMap occurrenceMap = new HashMap();
                for(ArrayList<Double> item : areaList){
                    int digit = item.get(1).intValue();
                    if(occurrenceMap.containsKey(digit)){
                        occurrenceMap.put(digit, (double)occurrenceMap.get(digit)+item.get(0));
                    }else{
                        occurrenceMap.put(digit, item.get(0));
                    }
                }
                
                //Find the key(digit) with the biggest occurance
                return (int)Collections.max(occurrenceMap.entrySet(),
                        new Comparator<Map.Entry<Integer,Double>>(){
                            @Override
                            public int compare(Map.Entry<Integer, Double> o1, Map.Entry<Integer, Double> o2) {
                                return o1.getValue() > o2.getValue()? 1:-1;
                            }
                        }).getKey();
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return -1;
    }
    
    /**
     * Create a list which has the first k number of images
     * These images have their weight evaluated
     * @param rmsList
     * @param rmsThreshold
     * @return
     */
    private List<ArrayList<Double>> weighted(ArrayList<ArrayList<Integer>> rmsList, int rmsThreshold){
        //create a sublist with the first K values
        List<ArrayList<Double>> areaList = new ArrayList<ArrayList<Double>>();
        
        try{
            double minRMSValue = Double.MAX_VALUE;
            //deep copy the first kValues from the rmsList
            for(ArrayList<Integer> image : rmsList){
                //get the rms value
                double rmsValue = image.get(0);
                if(rmsValue <= rmsThreshold){
                    ArrayList<Double> temp = new ArrayList<Double>();
                    double digit = image.get(1);
                    temp.add(rmsValue);
                    temp.add(digit);
                    areaList.add(temp);
                }else{
                    if(minRMSValue > rmsValue){
                        minRMSValue = rmsValue;
                    }
                }
            }
            
            if(areaList.size() > 0){
                //find the next value in the rms list which has not been included in the kList. This would by the same as the k+1 value in the k-nn algorithm
                double kValuePlusOneRMS = rmsList.get(areaList.size()).get(0);
                
                for(ArrayList<Double> image : areaList){
                    double imageRMS = image.get(0);
                    double newValue = (((double)kValuePlusOneRMS)/imageRMS) - 1;
                    newValue = Math.pow(newValue, 2);
                    image.set(0, newValue);
                }
                
            }else{
                if(this.rmsThresholdIsTooSmall > minRMSValue){
                    this.rmsThresholdIsTooSmall = minRMSValue;
                }
            }
        }
        catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return areaList;
    }
    
    /**
     * Create a list which has the first k number of images
     * These images have their weight evaluated
     * @param rmsList
     * @param kValue
     * @return
     */
    private List<ArrayList<Double>> binary(ArrayList<ArrayList<Integer>> rmsList, int rmsThreshold){
        //create a sublist with the first K values
        List<ArrayList<Double>> kList = new ArrayList<ArrayList<Double>>();
        
        try{
            double minRMSValue = Double.MAX_VALUE;
            //deep copy the first kValues from the rmsList
            for(ArrayList<Integer> image : rmsList){
                //get the rms value
                double rmsValue = image.get(0);
                if(rmsValue <= rmsThreshold){
                    ArrayList<Double> temp = new ArrayList<Double>();
                    double digit = image.get(1);
                    temp.add(1.0);
                    temp.add(digit);
                    kList.add(temp);
                } else{
                    if(minRMSValue > rmsValue){
                        minRMSValue = rmsValue;
                    }
                }
            }
            
//            //Test if there are
//            if(kList.size() > 0){
//                Set<Double> dub = new HashSet<Double>();
//                for (ArrayList<Double> im:kList) {
//                    if (dub.contains(im.get(0))) {
//                        System.out.println("Double found");
//                    }
//                    dub.add(im.get(0));
//                }
//            }
            
            
            if(kList.isEmpty()){
                if(this.rmsThresholdIsTooSmall > minRMSValue){
                    this.rmsThresholdIsTooSmall = minRMSValue;
                }
            }
        }
        catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return kList;
    }
    
    /**
     * method used to sort a multidimensional arraylist of Integer
     * @param columnIndex column which will be used for sorting
     * @param list list which will be sorted
     */
    private void sort(int columnIndex, ArrayList<ArrayList<Integer>> list) {
        Comparator<ArrayList<Integer>> myComparator = new Comparator<ArrayList<Integer>>() {
            @Override
            public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
                return o1.get(columnIndex).compareTo(o2.get(columnIndex));
            }
        };
        Collections.sort(list, myComparator);
    }
    
}

